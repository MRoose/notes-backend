CREATE TABLE IF NOT EXISTS `note` (
    `id` UUID NOT NULL,
    `title` VARCHAR(255),
    `content` VARCHAR(255),
    `created` TIMESTAMP NOT NULL,
    `updated` TIMESTAMP NOT NULL,
    PRIMARY KEY (id)
);
INSERT INTO note (
    id,
    title,
    content,
    created,
    updated
) VALUES (
    'df2d5bd0-eb62-4a24-97f2-c2821a4b7dcf',
    'Note1',
    'Content1',
    '2024-02-04 22:00:01',
    '2024-02-04 22:00:01'
);
INSERT INTO note (
    id,
    title,
    content,
    created,
    updated
) VALUES (
    '5f5eb193-b06c-491b-bf1a-5e803bdc4920',
    'Note2',
    'Content2',
    '2024-02-04 22:00:02',
    '2024-02-04 22:00:02'
);