package com.github.mroose.backend.service;

import com.github.mroose.backend.model.Note;
import com.github.mroose.backend.repository.NoteRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalDateTime;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class NoteService {

    private final NoteRepository noteRepository;

    public Flux<Note> getNotes() {
        return noteRepository.findAll();
    }

    public Mono<Note> createNote(Note note) {
        note.setId(UUID.randomUUID());
        note.setCreated(LocalDateTime.now());
        note.setUpdated(LocalDateTime.now());
        return noteRepository.save(note);
    }

    public Mono<Note> getNote(UUID id) {
        return noteRepository.findById(id);
    }

    public Mono<Note> updateNote(UUID id, Note note) {
        return noteRepository
                .findById(id)
                .map(n -> {
                    n.setTitle(note.getTitle());
                    n.setContent(note.getContent());
                    n.setUpdated(LocalDateTime.now());
                    return n;
                })
                .flatMap(noteRepository::save);
    }

    public Mono<Void> deleteNote(UUID id) {
        return noteRepository.deleteById(id);
    }
}