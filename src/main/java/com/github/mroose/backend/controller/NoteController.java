package com.github.mroose.backend.controller;

import com.github.mroose.backend.model.Note;
import com.github.mroose.backend.service.NoteService;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.UUID;

@RestController
@RequestMapping("/api/v1/notes")
@Tag(name = "notes")
@RequiredArgsConstructor
public class NoteController {

    private final NoteService noteService;

    @GetMapping()
    public Flux<Note> getNotes() {
        return noteService.getNotes();
    }

    @PostMapping()
    public Mono<Note> createNote(@RequestBody Note note) {
        return noteService.createNote(note);
    }

    @GetMapping("/{id}")
    public Mono<Note> getNote(@PathVariable UUID id) {
        return noteService.getNote(id);
    }

    @PutMapping("/{id}")
    public Mono<Note> updateNote(@PathVariable UUID id, @RequestBody Note note) {
        return noteService.updateNote(id, note);
    }

    @DeleteMapping("/{id}")
    public Mono<Void> deleteNote(@PathVariable UUID id) {
        return noteService.deleteNote(id);
    }
}