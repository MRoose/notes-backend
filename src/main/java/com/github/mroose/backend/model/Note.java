package com.github.mroose.backend.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
@Table(name = "note")
public class Note {

    @Id
    private UUID id;
    private String title;
    private String content;
    private LocalDateTime created;
    private LocalDateTime updated;
}