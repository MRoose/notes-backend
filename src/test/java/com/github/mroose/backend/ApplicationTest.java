package com.github.mroose.backend;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class ApplicationTest {

    @Test
    void firstTest() {
        var expected = "test";
        var actual = "test";
        assertEquals(expected, actual);
    }

    @Test
    void secondTest() {
        var expected = 100;
        var actual = 10*10;
        assertEquals(expected, actual);
    }
}